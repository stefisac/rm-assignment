#include <stdio.h>  
  
 #define MAX_ELEMENTS_LIMIT 100  
  
/* 
 * function signatures 
 */  
 float computeMean (float[],int);  
 float computeMedian (float[],int);  
 float computeMode (float[],int);  
  
int main() {  
    printf ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");  
    printf ("XXXXX    Mean / Mode / Median calculator    XXXXX\n");  
    printf ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");  
      
   /* 
    * variable declaration's 
    */  
    int i, nTotalNumbers, nUserChoice;  
    float anUserElements[MAX_ELEMENTS_LIMIT], nMean, nMedian, nMode;  
      
   /* 
    * take number of elements 
    */  
    printf("Enter No of Elements\n");  
    scanf("%d", &nTotalNumbers);  
      
   /* 
    * store user input numbers 
    */  
    printf("Enter Elements\n");  
    for(i=0; i<nTotalNumbers; i++) {  
        scanf("%f",&anUserElements[i]);  
    }  
    do {  
        printf("\n\tEnter Choice\n\t1.Mean\n\t2.Median\n\t3.Mode\n\t4.Exit\n");  
        scanf("%d",&nUserChoice);  
        switch(nUserChoice) {  
            case 1: nMean = computeMean(anUserElements,nTotalNumbers);  
                printf("\n\tMean = %f\n",nMean);  
                break;  
            case 2: nMedian = computeMedian(anUserElements,nTotalNumbers);  
                printf("\n\tMedian = %f\n",nMedian);  
                break;  
            case 3: nMode=computeMode(anUserElements,nTotalNumbers);  
                printf("\n\tMode = %f\n",nMode);  
                break;  
            case 4:   
                break;  
            default:printf("No Matching Option Found\n");  
                break;  
        }  
    }  
    while(nUserChoice!=4);  
    printf ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");  
    return 0;  
}  
  
/*************************************************************** 
 Function Name   : computeMean 
 Purpose         : find nMean 
 Return Value    : Mean 
 Return Type     : float 
 ***************************************************************/  
float computeMean(float anUserElements[],int nTotalNumbers) {  
    int i;  
    float sum=0;  
    for(i=0; i<nTotalNumbers; i++) {  
        sum = sum + anUserElements[i];  
    }  
    return (sum/nTotalNumbers);  
}  
  
/*************************************************************** 
 Function Name   : computeMedian 
 Purpose         : find Median 
 Return Value    : Median 
 Return Type     : Float 
 ***************************************************************/  
float computeMedian(float a[],int nTotalNumbers) {  
    float temp;  
    int i,j;  
    for(i=0; i<nTotalNumbers; i++) {  
        for(j=i+1; j<nTotalNumbers; j++) {  
            if(a[i]>a[j]) {  
                temp=a[j];  
                a[j]=a[i];  
                a[i]=temp;  
            }  
        }  
    }  
    if(nTotalNumbers%2==0) {  
        return (a[nTotalNumbers/2]+a[nTotalNumbers/2-1])/2;  
    }  
    else {  
        return a[nTotalNumbers/2];  
    }  
}  
  
/*************************************************************** 
 Function Name   : computeMode 
 Purpose         : find Mode 
 Return Value    : Mode 
 Return Type     : Float 
 ***************************************************************/  
float computeMode(float a[],int nTotalNumbers) {  
    return (3*computeMedian(a,nTotalNumbers)-2*computeMean(a,nTotalNumbers));  
}  