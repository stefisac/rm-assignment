#!/bin/sh

echo "Main program is running"
cat /dev/null > output.dat
echo '\n' "Please enter the input filename : " 
read ipFilename

while true; do

echo '\n' "Please choose any of the below option " '\n' "A for Mean Median Mode " '\n' "B for Range, InterQuarter Range and Standard Deviation " '\n' "G for Graphs in libreoffice "'\n' "X for exit "
read readOption

if  ( [ "$readOption" = "A" ] || [ "$readOption" = "B" ] )
then
	echo -n "Valid entries "
fi

case "$readOption" in
  A)
    echo "Calling Mean Median and Mode"
    ./fmmm
    ;;
  B)
    echo "Calling Range InterQueRange Standard Deviation and Coefficient of varience"
    ./fris3 $ipFilename
    ;;
  G) 
   echo "Calling libreoffice "
   libreoffice --calc /home/mtech/Desktop/RM/rmassignment.ods
    ;;
  X)
    echo '\n' "Good Bye "
    break
    ;;
  *)
	echo '\n' "Invalid entries, please try again "
    ;;
esac

#if ( [ "$readOption" <> "X" ] && [ "$readOption" <> "A" ] && [ "$readOption" <> "B" ] )
#then
#	echo '\n' "Invalid entries, please try again "
#fi
done
